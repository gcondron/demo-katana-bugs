﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Owin;
using Nancy;

namespace Demo.Bugs.Web
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
//            this.RequiresAuthentication();

            Get["/"] = _ =>
            {
                var owinEnvironment = (IDictionary<string, object>)this.Context.Items["OWIN_REQUEST_ENVIRONMENT"];
                var owinCtx = new OwinContext(owinEnvironment);

//                if (this.Context.CurrentUser == null)
//                {
//                    owinCtx.Authentication.Challenge("Google");
//                    return HttpStatusCode.Unauthorized;
//
//                    owinCtx.Authentication.SignIn(new ClaimsIdentity[]{new ClaimsIdentity(new []{new Claim("foo", "bar"), }, "Application"), });
//                }

                var model = new { title = "We've Got Issues..." };
                return View["home", model];
            };
        }
    }
}