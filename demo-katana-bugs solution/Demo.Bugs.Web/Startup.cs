﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Nancy;
using Nancy.Conventions;
using Nancy.Owin;
using Owin;

namespace Demo.Bugs.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
//            // configure local cookie authentication
//            app.UseCookieAuthentication(new CookieAuthenticationOptions
//            {
//                AuthenticationType = "Application",
//                CookieName = ".AspNet.Application",
//                LoginPath = "/",
//                LogoutPath = "/logout"
//            });
//
////            app.SetDefaultSignInAsAuthenticationType("External");   // QUESTION: what is this?
//
//            // temporary cookie used between google and local application
//            app.UseCookieAuthentication(new CookieAuthenticationOptions
//            {
//                AuthenticationType = "External",
//                AuthenticationMode = AuthenticationMode.Passive,
//                ExpireTimeSpan = TimeSpan.FromMinutes(5)
//            });
//
//            app.UseGoogleAuthentication(new GoogleAuthenticationOptions
//            {
//                AuthenticationType = "Google",  //maps to what's specificied in my module challenge 
//                SignInAsAuthenticationType = "Application",
//                Provider = new GoogleAuthenticationProvider(),  // this is the default value: only matters of you need to hook events (like onauthenticated)
//                ReturnEndpointPath = "/signin-google"   // this is the default value: path that google is instructed to return to after grant
//            });

            app.MapSignalR();

            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("bugs", "api/{Controller}");
            app.UseWebApi(config);
            
            //note: NancyFx is a greedy handler - therefore need to either put it last or map a different pipeline for it
            app.UseNancy(options =>
            {
                options.Bootstrapper = new CustomBootstrapper();
            });
        }

        public class CustomBootstrapper : DefaultNancyBootstrapper
        {
            protected override void ConfigureConventions(NancyConventions nancyConventions)
            {
                base.ConfigureConventions(nancyConventions);

                Conventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("scripts", "Scripts"));
            }
        }
    }
}